from infrastructure.event_queue import EventQueue
from infrastructure.event_queue_subscriber import EventQueueSubscriber
from infrastructure.event_store import JsonFileStore, EventStore
from infrastructure.transcoders import ObjectJSONEncoder, ObjectJSONDecoder


async def init_model(app):
    conf = app['config']
    jfs = JsonFileStore(store_path=conf['event_store_path'],
                        json_encoder_class=ObjectJSONEncoder,
                        json_decoder_class=ObjectJSONDecoder)
    es = EventStore(jfs)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)
    app['jfs'] = jfs
    app['es'] = es
    app['eq'] = eq
    app['eqs'] = eqs


async def close_model(app):
    app['eqs'].close()
    app['eq'].close()
