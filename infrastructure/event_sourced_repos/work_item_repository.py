from infrastructure.event_sourced_repos.event_tracking_repository import EventTrackingRepository
from kanban.domain.model import workitem


# Idea - maintain an in-memory store of uncommitted events that just stores *all* events.
#        Repositories can query this to get a back-history of changes for saving. When they
#        save they remove events from this global store.  The global store can give warning
#        if it is destroyed with pending, unsaved, events.  This way, all we need is a
#        save() method on the repository.


# Idea 2 - this concrete repository is an event-store. We can also implement a unit-of-work
#          with the same interface as the event-store. An in-memory transactional wrapper.

# Idea 3 - newly created Aggregates *must* not be shared between repositories active
#          created instantiated under different units of work. So, we can have a global
#          event queue for capturing events from new aggregates, and per-repository
#          event queues for capturing events from rehydrated aggregates returned by
#          the repository.  Much the same argument applies to interning of aggregates
#          in an identity map.

# Idea 4 - Add a instance_id into events so we can distinguish between events coming from
#          different instantiations of the same aggregate produced by different respositories
#          in different concurrent contexts. The instance_id can be stripped out of the persisted
#          events.  The instance id should be based on an incrementing static counter in the entity
#          base class.  Either a) The identity map should hold strong references to WorkItems -
#          which is simple but could lead to large space leaks - or we use weak references and
#          store both tuples of (work_item_id, instance_id)


class WorkItemRepository(workitem.Repository, EventTrackingRepository):
    """Concrete repository for WorkItems in terms of an event store.
    """

    def __init__(self, transient_event_queue, persistent_event_store, **kwargs):
        """Initialize.

        It is assumed that the concatenation of the event streams in persistent_event_store
        and transient_event_queue contain all events in which this repository has an interest.

        Args:
            transient_event_queue: An EventQueue from which unsaved and future events
                pertaining to aggregates which will be, or have been, put into this
                repository, can be retrieved. Essentially this is the collection from
                where "present" (happened, but unsaved) and future (yet to happen)
                events can be retrieved.

            persistent_event_store: An EventStore containing past events.

            **kwargs: Arguments to be forwarded to other base classes.
        """
        super().__init__(transient_event_queue=transient_event_queue,
                         persistent_event_store=persistent_event_store,
                         mutator=workitem.mutate,
                         **kwargs)

    def put(self, work_item):
        """Make the repository aware of an aggregate.

        This method is idempotent.

        Args:
            work_item: The aggregate to register with the repository.
        """
        self._register(work_item)

    def work_item_ids(self):
        """An iterable series of work-item IDs."""
        return self._extant_aggregate_ids()

    def work_item_with_id(self, work_item_id):
        """Retrieve a WorkItem by ID.

        Args:
            work_item_id: An UniqueId corresponding to the WorkItem.

        Returns:
            The WorkItem.

        Raises:
            ValueError: If a WorkItem with the id could not be found.
        """
        return self._aggregate_with_id(work_item_id)

    def work_items_with_ids(self, work_item_ids=None):
        """Retrieve multiple WorkItems by id.

        Args:
            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                will be returned.

        Returns:
            An iterable series of WorkItems.
        """
        return self._aggregates_with_ids(work_item_ids)

    def work_items_with_name(self, name, work_item_ids=None):
        """Retrieve WorkItems by name.

        Args:
            name (str): Work items with names equal to this value will
                be included in the result.

            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                matching name will be returned.

        Returns:
            An iterable series of work items.
        """
        return filter(lambda work_item: work_item.name == name,
                      self.work_items_with_ids(work_item_ids))

    def discard(self, work_item_id):
        """Delete a WorkItem permanently.

        Args:
            work_item_id: The ID of the work item to be deleted.

        Raises:
            ValueError: If there is no work item corresponding to the ID.
            ConstraintError: If the WorkItem could not be deleted.
        """
        work_item = self.work_item_with_id(work_item_id)
        self._unregister(work_item)
        work_item._discard()

    def _aggregate_root_entity_class(self):
        return workitem.WorkItem
