import pytest

from infrastructure.event_queue import EventQueue
from infrastructure.event_queue_subscriber import EventQueueSubscriber
from infrastructure.event_sourced_repos.work_item_repository import WorkItemRepository
from infrastructure.event_store import EventStore
from infrastructure.rebaser import Rebaser, rebase
from infrastructure.unit_of_work import UnitOfWork, ConflictError
from kanban.domain.model.workitem import register_new_work_item


def test_rebaser_1():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()
    w1 = register_new_work_item("WorkItem 1", None, "The first work item")
    u1.using(WorkItemRepository).put(w1)
    u1.commit()

    u2 = UnitOfWork(eq, es)
    u3 = UnitOfWork(eq, es)

    u2.begin()
    u3.begin()

    w2 = u2.using(WorkItemRepository).work_item_with_id(w1.id)
    w3 = u3.using(WorkItemRepository).work_item_with_id(w1.id)

    assert w1.id == w2.id == w3.id
    assert (w1 is not w2) and (w2 is not w3)

    w2.name = "Fork 2"
    w3.name = "Fork 3"

    u2.commit()

    with pytest.raises(ConflictError) as exc_info:
        u3.commit()
    assert len(exc_info.value.events) == 1

    u4 = UnitOfWork(eq, es)
    u4.begin()
    rebase(u4, exc_info.value.events)
    u4.commit()

    u5 = UnitOfWork(eq, es)
    u5.begin()
    w5 = u5.using(WorkItemRepository).work_item_with_id(w1.id)
    assert w5.name == "Fork 3"
    u5.commit()

    eqs.close()
    eq.close()

