import pytest

from infrastructure.event_queue import EventQueue
from infrastructure.event_queue_subscriber import EventQueueSubscriber
from infrastructure.event_sourced_repos.board_repository import BoardRepository
from infrastructure.event_sourced_repos.work_item_repository import WorkItemRepository
from infrastructure.event_store import EventStore
from infrastructure.unit_of_work import UnitOfWork, ConflictError
from kanban.domain.model.board import start_project
from kanban.domain.model.workitem import register_new_work_item


def test_no_events_persisted_before_unit_of_work_committed():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    b1.add_new_column("To read", 20)
    b1.add_new_column("Reading", 3)
    b1.add_new_column("Read", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    assert len(events) == 0
    u1.commit()

    eqs.close()
    eq.close()


def test_all_events_persisted_after_unit_of_work_committed():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    b1.add_new_column("To read", 20)
    b1.add_new_column("Reading", 3)
    b1.add_new_column("Read", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    u1.commit()

    eqs.close()
    eq.close()

    assert len(events) == 9


def test_no_events_removed_from_event_queue_before_unit_of_work_committed():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    b1.add_new_column("To read", 20)
    b1.add_new_column("Reading", 3)
    b1.add_new_column("Read", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    assert len(eq) == 9
    u1.commit()

    eqs.close()
    eq.close()


def test_all_events_removed_from_event_queue_after_unit_of_work_committed():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    b1.add_new_column("To read", 20)
    b1.add_new_column("Reading", 3)
    b1.add_new_column("Read", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    u1.commit()
    assert len(eq) == 0

    eqs.close()
    eq.close()


def test_overlapping_units_of_work_are_isolated_when_both_are_committed():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u2 = UnitOfWork(eq, es)

    u1.begin()
    u2.begin()

    b1 = start_project("Project 1", "Reading project")
    b2 = start_project("Project 2", "Writing project")

    u1.using(BoardRepository).put(b1)
    u2.using(BoardRepository).put(b2)

    b1.add_new_column("To read", 20)
    b2.add_new_column("To write", 17)

    b1.add_new_column("Reading", 3)
    b2.add_new_column("Writing", 3)

    b1.add_new_column("Read", None)
    b2.add_new_column("Written", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    w2 = register_new_work_item(name="My holiday in the English Lake District")

    u1.using(WorkItemRepository).put(w1)
    u2.using(WorkItemRepository).put(w2)

    b1.schedule_work_item(w1)
    b2.schedule_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.retire_work_item(w1)
    b2.retire_work_item(w2)

    aggregate_ids_1 = {b1.id, w1.id}
    aggregate_ids_2 = {b2.id, w2.id}

    assert len(eq) == 18
    assert len(events) == 0
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    assert eq_aggregate_ids == aggregate_ids_1 | aggregate_ids_2

    u1.commit()

    assert len(eq) == 9
    assert len(events) == 9
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    event_aggregate_ids = {event['attributes']['aggregate_id'] for event in events}
    assert event_aggregate_ids == aggregate_ids_1
    assert eq_aggregate_ids == aggregate_ids_2
    assert eq_aggregate_ids.isdisjoint(event_aggregate_ids)

    u2.commit()

    assert len(eq) == 0
    assert len(events) == 18
    event_aggregate_ids = {event['attributes']['aggregate_id'] for event in events}
    assert event_aggregate_ids == aggregate_ids_1 | aggregate_ids_2

    eqs.close()
    eq.close()


def test_pending_events_uncommitted_when_unit_of_work_aborted():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    b1.add_new_column("To read", 20)
    b1.add_new_column("Reading", 3)
    b1.add_new_column("Read", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    assert len(eq) == 9
    assert len(events) == 0

    uncommitted_events = u1.abort()

    assert len(eq) == 0
    assert len(events) == 0
    assert len(uncommitted_events) == 9

    eqs.close()
    eq.close()


def test_overlapping_units_of_work_are_isolated_when_one_committed_and_one_aborted():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u2 = UnitOfWork(eq, es)

    u1.begin()
    u2.begin()

    b1 = start_project("Project 1", "Reading project")
    b2 = start_project("Project 2", "Writing project")

    u1.using(BoardRepository).put(b1)
    u2.using(BoardRepository).put(b2)

    b1.add_new_column("To read", 20)
    b2.add_new_column("To write", 17)

    b1.add_new_column("Reading", 3)
    b2.add_new_column("Writing", 3)

    b1.add_new_column("Read", None)
    b2.add_new_column("Written", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    w2 = register_new_work_item(name="My holiday in the English Lake District")

    u1.using(WorkItemRepository).put(w1)
    u2.using(WorkItemRepository).put(w2)

    b1.schedule_work_item(w1)
    b2.schedule_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.retire_work_item(w1)
    b2.retire_work_item(w2)

    aggregate_ids_1 = {b1.id, w1.id}
    aggregate_ids_2 = {b2.id, w2.id}

    assert len(eq) == 18
    assert len(events) == 0
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    assert eq_aggregate_ids == aggregate_ids_1 | aggregate_ids_2

    uncommitted_events_1 = u1.abort()

    assert len(eq) == 9
    assert len(events) == 0
    assert len(uncommitted_events_1) == 9
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    uncommitted_aggregate_ids_1 = {event.aggregate_id for event in uncommitted_events_1}
    assert uncommitted_aggregate_ids_1 == aggregate_ids_1
    assert eq_aggregate_ids == aggregate_ids_2
    assert eq_aggregate_ids.isdisjoint(uncommitted_aggregate_ids_1)

    u2.commit()

    assert len(eq) == 0
    assert len(events) == 9
    event_aggregate_ids = {event['attributes']['aggregate_id'] for event in events}
    assert event_aggregate_ids == aggregate_ids_2

    eqs.close()
    eq.close()


def test_overlapping_units_of_work_are_isolated_when_both_aborted():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u2 = UnitOfWork(eq, es)

    u1.begin()
    u2.begin()

    b1 = start_project("Project 1", "Reading project")
    b2 = start_project("Project 2", "Writing project")

    u1.using(BoardRepository).put(b1)
    u2.using(BoardRepository).put(b2)

    b1.add_new_column("To read", 20)
    b2.add_new_column("To write", 17)

    b1.add_new_column("Reading", 3)
    b2.add_new_column("Writing", 3)

    b1.add_new_column("Read", None)
    b2.add_new_column("Written", None)

    w1 = register_new_work_item(name="Swallows and Amazons")
    w2 = register_new_work_item(name="My holiday in the English Lake District")

    u1.using(WorkItemRepository).put(w1)
    u2.using(WorkItemRepository).put(w2)

    b1.schedule_work_item(w1)
    b2.schedule_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.advance_work_item(w1)
    b2.advance_work_item(w2)

    b1.retire_work_item(w1)
    b2.retire_work_item(w2)

    aggregate_ids_1 = {b1.id, w1.id}
    aggregate_ids_2 = {b2.id, w2.id}

    assert len(eq) == 18
    assert len(events) == 0
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    assert eq_aggregate_ids == aggregate_ids_1 | aggregate_ids_2

    uncommitted_events_1 = u1.abort()

    assert len(eq) == 9
    assert len(events) == 0
    assert len(uncommitted_events_1) == 9
    eq_aggregate_ids = {event.aggregate_id for event in eq}
    uncommitted_aggregate_ids_1 = {event.aggregate_id for event in uncommitted_events_1}
    assert uncommitted_aggregate_ids_1 == aggregate_ids_1
    assert eq_aggregate_ids == aggregate_ids_2
    assert eq_aggregate_ids.isdisjoint(uncommitted_aggregate_ids_1)

    uncommitted_events_2 = u2.abort()

    assert len(eq) == 0
    assert len(events) == 0
    assert len(uncommitted_events_2) == 9
    uncommitted_aggregate_ids_2 = {event.aggregate_id for event in uncommitted_events_2}
    assert uncommitted_aggregate_ids_2 == aggregate_ids_2
    assert uncommitted_aggregate_ids_1.isdisjoint(uncommitted_aggregate_ids_2)

    eqs.close()
    eq.close()


def test_column_mutation_events_are_persisted():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    c1a = b1.add_new_column("To read", 20)
    c2a = b1.add_new_column("Reading", 3)
    c3a = b1.add_new_column("Read", None)

    c1a.name = "For reading"
    c1a.wip_limit = 10

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    u1.commit()

    eqs.close()
    eq.close()

    assert len(events) == 11
    assert len(eq) == 0


def test_column_mutation_events_are_reconstituted():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    c1a = b1.add_new_column("To read", 20)
    c2a = b1.add_new_column("Reading", 3)
    c3a = b1.add_new_column("Read", None)

    c1a.name = "For reading"
    c2a.wip_limit = 10
    c3a.wip_limit = 19

    w1 = register_new_work_item(name="Swallows and Amazons")
    u1.using(WorkItemRepository).put(w1)

    b1.schedule_work_item(w1)
    b1.advance_work_item(w1)
    b1.advance_work_item(w1)
    b1.retire_work_item(w1)

    u1.commit()

    eqs.close()
    eq.close()

    assert len(eq) == 0

    br = BoardRepository(eq, es)
    b2 = br.board_with_id(b1.id)

    columns = list(b2.columns())

    assert columns[0].name == "For reading"
    assert columns[1].wip_limit == 10
    assert columns[2].wip_limit == 19


def test_forking_aggregate_must_cause_commit_failure():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    c1a = b1.add_new_column("To read", 20)
    c2a = b1.add_new_column("Reading", 3)
    c3a = b1.add_new_column("Read", None)

    c1a.name = "For reading"
    c2a.wip_limit = 10
    c3a.wip_limit = 19

    u1.commit()

    u2 = UnitOfWork(eq, es)
    u3 = UnitOfWork(eq, es)

    u2.begin()
    u3.begin()

    b2 = u2.using(BoardRepository).board_with_id(b1.id)
    b3 = u3.using(BoardRepository).board_with_id(b1.id)

    assert b1.id == b2.id == b3.id
    assert (b1 is not b2) and (b2 is not b3)

    b2.name = "Fork 2"
    b3.name = "Fork 3"

    u2.commit()

    with pytest.raises(ConflictError) as exc_info:
        u3.commit()
    assert len(exc_info.value.events) == 1

    eqs.close()
    eq.close()


def test_unit_of_work_read_isolation():
    events = []

    es = EventStore(events)
    eq = EventQueue()
    eqs = EventQueueSubscriber(eq)

    u1 = UnitOfWork(eq, es)
    u1.begin()

    b1 = start_project("Project 1", "The first project")
    u1.using(BoardRepository).put(b1)

    c1a = b1.add_new_column("To read", 20)
    c2a = b1.add_new_column("Reading", 3)
    c3a = b1.add_new_column("Read", None)

    c1a.name = "For reading"
    c2a.wip_limit = 10
    c3a.wip_limit = 19

    u1.commit()

    u2 = UnitOfWork(eq, es)
    u3 = UnitOfWork(eq, es)

    u2.begin()
    u3.begin()

    r2 = u2.using(BoardRepository)
    r3 = u3.using(BoardRepository)

    b2 = r2.board_with_id(b1.id)
    b2.name = "Project 2"

    b3 = r3.board_with_id(b1.id)
    assert b3.name == "Project 1"

    eqs.close()
    eq.clear()
    eq.close()

